OC.L10N.register(
    "amx_filesharing",
    {
        "ifContinue": "Si continuas, {shareName} no tendrá más acceso a este archivo y tampoco las personas" +
           " que hayan recibido una invitación de su parte",
        "wantDelete": "¿Quieres elimiar el acceso a {shareName}?",
        "cancel": "Cancelar",
        "remove": "Eliminar",
        "writeHere" :  "Escribe aquí un mensaje (opcional)",
        "share":"Compartir",
        "to": "Para",
        "expireDate": "Selecciona la vigencia de tu archivo",
        "reShare": "Puede volver a compartir",
        "linkToShare": "Liga para compartir",
        "copyLink": "Copiar liga",
        "deleteLink": "Eliminar liga",
        "configLink": "Configurar liga",
        "anyCanSee": "Cualquier persona con el link puede ver este folder",
        "back": "Regresar",
        "writeName": "Ingresa Email",
        "inputTooShort": "Ingresa un email válido",
        "noResults": "Ingresa un email",
        "loadingMore": "Cargando más resultados...",
        "errorLoading": "Ingresa un Email",
        "searching": "Buscando contactos..."

    },
    "nplurals=2; plural=(n != 1);");
