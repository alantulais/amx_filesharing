OC.L10N.register(
    "amx_filesharing",
    {
        "ifContinue": "If you continue, {shareName} will not longer have access to this file" +
        " and neither will the people who have received an invitation from him.",
        "wantDelete": "Do you want to remove access to {shareName}?",
        "cancel": "Cancel",
        "remove": "Delete",
        "writeHere" :  "Write a message here (optional)",
        "share":"Share",
        "to": "To",
        "expireDate": "Select the expiration date of your file",
        "reShare": "You can reshare",
        "linkToShare": "Link to share",
        "copyLink": "Copy link",
        "deleteLink": "Delete link",
        "configLink": "Configure link",
        "anyCanSee": "Anyone with the link can see this folder",
        "back": "Back",
        "writeName": "Enter an Email",
        "inputTooShort": "Type a valid Email",
        "noResults": "Type an Email",
        "loadingMore": "Loading more results...",
        "errorLoading": "Type an Email",
        "searching": "Searching for contacts..."
    },
    "nplurals=2; plural=(n != 1);");
