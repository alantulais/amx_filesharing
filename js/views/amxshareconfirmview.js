(function() {

    if (!OCA.AmxSharing) {
        OCA.AmxSharing = {};
    }

    var TEMPLATE = '<div class="shareSubject"><textarea id="shareAreaAmx" placeholder="{{writeHere}}"></textarea></div>' +
        '<div class="footer-confirm">' +
            '<div class="progress-bar-wrap"><div class="progress-bar-back hidden"><div class="progress-bar-front"></div></div></div>' +
            '<span class="icon-loading-small hidden"></span>' +
            '<button id="shareBtnAmx" class="shareBtn">{{share}}</button>' +
        '</div> ';

    var AmxShareConfirmView = OC.Backbone.View.extend({

        _templates: {},

        configModel: undefined,

        events: {
            'click #shareBtnAmx': 'onConfirmShare'
        },
        initialize: function(options) {

            // Integrity validations

            this.model.on('fetchError', function() {
                OC.Notification.showTemporary(t('core', 'Share details could not be loaded for this item.'));
            });

            if(!_.isUndefined(options.configModel)) {
                this.configModel = options.configModel;
            } else {
                throw 'missing OC.Share.ShareConfigModel';
            }

        },

        render: function(){

            var $self = this;

            // Create template

            var baseTemplate = this._getTemplate('base', TEMPLATE);

            this.$el.html(baseTemplate({
                writeHere: t('amx_filesharing','writeHere'),
                share: t('amx_filesharing','share')
            }));

            this.delegateEvents();

        },


        /* ELEMENT EVENTS
        ------------------------------------------------- */

        onConfirmShare: function(event){

            var view = this;

            this.model.trigger("confirmShare", this.model, view.$el.find('#shareAreaAmx').val());

        },

        /* VIEW HELPERS
        ------------------------------------------------- */

        _getTemplate: function (key, template) {
            if (!this._templates[key]) {
                this._templates[key] = Handlebars.compile(template);
            }
            return this._templates[key];
        }

    });

    OCA.AmxSharing.ShareConfirmView = AmxShareConfirmView;

})();

