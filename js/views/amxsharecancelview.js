(function() {

    if (!OCA.AmxSharing) {
        OCA.AmxSharing = {};
    }

    var TEMPLATE =
        '<div class="modal-header">  '  +
            '<a class="modal-close" href="#"><span class="icon icon-close"></span></a>'  +
            '<h2>{{wantDelete}} </h2>  '  +
        '</div>  '  +
        '<div class="modal-body"><div>' +
            '<p>{{ifContinue}} </p>' +
            '<div class="footer-cancel"><button class="shareBtnWhite" id="unshareCancelBtnAmx">{{cancel}}</button> <button class="shareBtn" id="unshareBtnAmx">{{remove}}</button></div>' +
        '</div></div>';

    var AmxShareCancelView = OC.Backbone.View.extend({

        _templates: {},

        _unshareData: {},

        configModel: undefined,

        events: {
            'click #unshareBtnAmx': 'onUnshare',
            'click #unshareCancelBtnAmx': 'goBack'
        },

        initialize: function(options) {

            var self = this;

            // Integrity validations

            this.model.on('fetchError', function() {
                OC.Notification.showTemporary(t('core', 'Share details could not be loaded for this item.'));
            });

            if(!_.isUndefined(options.configModel)) {
                this.configModel = options.configModel;
            } else {
                throw 'missing OC.Share.ShareConfigModel';
            }

            // Events

            this.model.on('onUnshare', function (model, li, shareId, shareName) {

                self._unshareData = {
                    li: li,
                    shareId: shareId,
                    shareName: shareName
                };

                $('#amxSharePopup').hide();
                $('#amxShareCancel').slideDown();

                self.render();

            }, this);

            // Binding

            _.bindAll(this,
                'goBack'
            );

        },

        render: function(){

            var self = this;

            // Create template

            var baseTemplate = this._getTemplate('base', TEMPLATE);

            this.$el.html(baseTemplate({
                ifContinue: t('amx_filesharing','ifContinue',{
                    shareName: self._unshareData.shareName
                }),
                wantDelete: t('amx_filesharing','wantDelete',{
                    shareName: self._unshareData.shareName
                }),
                cancel: t('amx_filesharing','cancel'),
                remove: t('amx_filesharing','remove')

            }));

            this.delegateEvents();

        },


        /* ELEMENT EVENTS
        ------------------------------------------------- */

        onUnshare: function(event){

            var self = this;

            var btns = $('.footer-cancel button');

            btns.prop('disabled', true);

            self.model.removeShare(self._unshareData.shareId)
                .done(function() {

                    self._unshareData.li.remove();

                    btns.prop('disabled', false);

                    self.goBack();

                })
                .fail(function() {
                    OC.Notification.showTemporary(t('core', 'Could not unshare'));
                    btns.prop('disabled', false);
                });

        },

        /* VIEW HELPERS
        ------------------------------------------------- */

        goBack : function(){

            var self = this;

            self._unshareData = {};

            $('#amxSharePopup').slideDown();
            $('#amxShareCancel').hide();

        },

        _getTemplate: function (key, template) {
            if (!this._templates[key]) {
                this._templates[key] = Handlebars.compile(template);
            }
            return this._templates[key];
        }

    });

    OCA.AmxSharing.ShareCancelDialogView = AmxShareCancelView;

})();

