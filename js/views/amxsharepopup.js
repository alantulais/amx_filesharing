(function() {

    if (!OCA.AmxSharing) {
        OCA.AmxSharing = {};
    }

    var TEMPLATE = '<div class="modal-content" id="amxSharePopup">'  +
                '<div class="modal-header">  '  +
                    '<a class="modal-close" href="#"><span class="icon icon-close"></span></a>'  +
                    '<h2><div style="background-image:url({{fileIconSrc}})" class="thumbnail"></div> {{fileName}}</h2>  '  +
                '</div>  '  +
                '<div class="modal-body">' +
                    '<div class="shareFormView subView"></div>' +
                    '<div class="shareLinkView hidden subView"></div>' +
                '</div>'  +
        '</div>' +
        '<div class="modal-content" style="display: none" id="amxShareCancel"></div>';

    var AmxSharePopup = OC.Backbone.View.extend({

        _templates: {},

        /** @type {OC.Share.ShareConfigModel} **/
        configModel: undefined,

        /** @type {Function} **/
        _template: undefined,


        events: {

            'click .modal-close' : 'onModalClose'
        },

        initialize: function(options) {

            var view = this;

            // Integrity validations

            this.model.on('fetchError', function() {
                OC.Notification.showTemporary(t('core', 'Share details could not be loaded for this item.'));
            });

            if(!_.isUndefined(options.configModel)) {
                this.configModel = options.configModel;
            } else {
                throw 'missing OC.Share.ShareConfigModel';
            }

            // Events

            this.model.on('createLinkShare', function (model, element) {

                view.createLinkShare(element);

            }, this);

            this.model.on('deleteLinkShare', function (model, element) {

                view.deleteLinkShare(element);

            }, this);

            this.model.on('change', function() {

                $(document).trigger('sharesChanged', view.model);

            }, this);

            // Subviews

            var subViewOptions = {
                model: this.model,
                configModel: this.configModel
            };

            var subViews = {
                shareDialogView: 'ShareDialogView',
                shareCancelDialogView: 'ShareCancelDialogView',
                shareLinkView: 'ShareLinkView'
            };

            for(var name in subViews) {
                var className = subViews[name];
                this[name] = _.isUndefined(options[name])
                    ? new OCA.AmxSharing[className](subViewOptions)
                    : options[name];
            }

            // Binding

            _.bindAll(this,
                'deleteView',
                'showShareConfig',
                'createLinkShare',
                'deleteLinkShare'
            );

        },

        render: function(){

            var $self = this;

            // Create template

            var baseTemplate = this._getTemplate('base', TEMPLATE);

            this.$el.html(baseTemplate({
                fileName: this.model.fileInfoModel.attributes.name,
                fileIconSrc: OC.MimeType.getIconUrl(this.model.fileInfoModel.attributes.mimetype)
            }));

            this.shareDialogView.$el = this.$el.find('#amxSharePopup .modal-body div.shareFormView');
            this.shareDialogView.render();

            this.shareLinkView.$el = this.$el.find('#amxSharePopup .modal-body div.shareLinkView');
            this.shareLinkView.render();

            this.shareCancelDialogView.$el = this.$el.find('#amxShareCancel');
            this.shareCancelDialogView.render();

            this.delegateEvents();

        },

        /* ELEMENT EVENTS
        ------------------------------------------------- */

        onModalClose: function(){

            OCA.AmxSharing.ShareLauncher.unlaunch();

        },

        /* VIEW HELPERS
        ------------------------------------------------- */

        deleteView: function () {

            this.remove();

        },

        createLinkShare: function(element){

            var self = this;

            var $checkBox = element;

            var $loading = $checkBox.siblings('.icon-loading-small');

            if(!$loading.hasClass('hidden')) { return false; }

            if (!this.model.get('linkShare').isLinkShare){

                // Create new link share

                $loading.removeClass('hidden');

                this.model.saveLinkShare({}, {
                    success: function () {

                        $loading.addClass('hidden');

                        self.showShareConfig();

                        $("a.footer-link-copylink").removeClass('copylink-noshare');

                        $("#linkCopyText").text(self.model.get('linkShare').link);

                    }
                });

            }else{

                $loading.addClass('hidden');

                self.showShareConfig();

            }


        },

        showShareConfig: function () {

            var $self = this;

            $self.shareDialogView.$el.hide();
            $self.shareLinkView.$el.slideDown();

        },

        deleteLinkShare: function (element) {

            var $self = this;

            $("a.footer-link-copylink").addClass('copylink-noshare');
            $("#linkCopyText").text('');

            $self.shareDialogView.$el.slideDown();
            $self.shareLinkView.$el.hide();

        },

        _getTemplate: function (key, template) {
            if (!this._templates[key]) {
                this._templates[key] = Handlebars.compile(template);
            }
            return this._templates[key];
        }


    });

    OCA.AmxSharing.SharePopup = AmxSharePopup;

    OCA.AmxSharing.ShareLauncher = {

            _modalView : undefined,

            launch: function (shareModel, configModel) {


                // Verify sharing permissions

                var resharingAllowed = shareModel.sharePermissionPossible();

                if(!resharingAllowed) {

                    //OC.Notification.showTemporary(t('core', 'Resharing is not allowed'));

                    //$('tbody#fileList tr.highlighted').removeClass('highlighted');

                    //return;
                }

                // Show popup

                $('body').append('<div id="myModal" class="modal"></div>');

                var modalView = new OCA.AmxSharing.SharePopup({
                    configModel: configModel,
                    model: shareModel
                });

                modalView.$el = $('div#myModal');

                $('div#myModal').click(function (event) {

                    if (event.target.id === "myModal"){

                        event.preventDefault();
                        event.stopPropagation();

                        modalView.onModalClose();

                        return false;

                    }

                });

                modalView.render();

                OCA.AmxSharing.ShareLauncher._modalView = modalView;

            },

            unlaunch: function(){

                OCA.AmxSharing.ShareLauncher._modalView.deleteView();

                OCA.AmxSharing.ShareLauncher._modalView = undefined;

                $('tbody#fileList tr.highlighted').removeClass('highlighted');

            }

    };

})();

