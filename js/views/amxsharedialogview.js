
$.fn.select2_v4 = $.fn.select2;

$.fn.select2 = window.select2_origin;

(function() {

    var PASSWORD_PLACEHOLDER_MESSAGE = t('core', 'Choose a password for the mail share');

    if (!OCA.AmxSharing) {
        OCA.AmxSharing = {};
    }

    var TEMPLATE = '<div class="shareForm"><div class="shareForm-oneline">' +

            // LABEL

            '<div class="shareForm-label">{{to}}: </div>' +

            // OPTIONS

            '<div class="shareForm-btn">' +
                    '<a href="#"><span class="icon icon-more"></span></a>' +
                     '<div id="popover-wrap">' +

                        '<div class="popovermenu bubble hidden menu">' +
                            '<ul>' +

                                // Resharing!

                                '{{#if isResharingAllowed}} {{#if sharePermissionPossible}} ' +

                                    '<li>' +
                                        '<span class="shareOption menuitem">' +
                                            '<input  id="canShare" type="checkbox" name="share" class="permissions checkbox" data-permissions="{{sharePermission}}" />' +
                                        '   <label for="canShare">{{canShareLabel}}</label>' +
                                        '</span>' +
                                    '</li>' +

                                '{{/if}} {{/if}}' +

                                // Edit permissions

                                '{{#if editPermissionPossible}}{{#unless isFolder}}' +
                                    '<li>' +
                                        '<span class="shareOption menuitem">' +
                                            '<input id="canEdit" type="checkbox" name="edit" class="permissions checkbox" data-permissions="{{editPermission}}" />' +
                                            '<label for="canEdit">{{canEditLabel}}</label>' +
                                        '</span>' +
                                    '</li>' +
                                '{{/unless}}{{/if}}' +

                                // Folder permissions

                                '{{#if isFolder}}' +

                                    '{{#if createPermissionPossible}}' +
                                        '<li>' +
                                            '<span class="shareOption menuitem">' +
                                            '<input id="canCreate" type="checkbox" name="create" class="permissions checkbox" data-permissions="{{createPermission}}"/>' +
                                            '<label for="canCreate">{{createPermissionLabel}}</label>' +
                                            '</span>' +
                                        '</li>' +
                                    '{{/if}}' +

                                    '{{#if updatePermissionPossible}}' +
                                        '<li>' +
                                            '<span class="shareOption menuitem">' +
                                                '<input id="canUpdate" type="checkbox" name="update" class="permissions checkbox" data-permissions="{{updatePermission}}"/>' +
                                                '<label for="canUpdate">{{updatePermissionLabel}}</label>' +
                                            '</span>' +
                                        '</li>' +
                                    '{{/if}}' +

                                    '{{#if deletePermissionPossible}}' +
                                        '<li>' +
                                            '<span class="shareOption menuitem">' +
                                                '<input id="canDelete" type="checkbox" name="delete" class="permissions checkbox" data-permissions="{{deletePermission}}"/>' +
                                                '<label for="canDelete">{{deletePermissionLabel}}</label>' +
                                            '</span>' +
                                        '</li>' +
                                    '{{/if}}' +

                                '{{/if}}' +

                                // Email protect


                                // Expiration date

                                '<li>' +
                                    '<span class="shareOption menuitem">' +
                                        '<input type="checkbox" name="expirationDate" class="expireDate checkbox" />' +

                                        '<label for="expirationDatePicker">{{expireDateLabel}}</label>' +

                                        '<div class="expirationDateContainer hidden">' +
                                        '    <label for="expirationDatePicker" class="hidden-visually" value="{{expirationDate}}">{{expirationLabel}}</label>' +
                                        '    <input id="expirationDatePicker" class="datepicker" type="text" placeholder="{{expirationDatePlaceholder}}" value="" />' +
                                        '</div>' +
                                '   </span>' +
                                '</li>' +

                            '</ul>' +
                        '</div>' +

                     '</div>' +
            '</div>' +


            // SELECT INPUT

            '<div class="shareForm-input"><div>' +
                '<select class="form-control" id="shareWithAmx" multiple="multiple"></select>' +
            '</div></div>' +


        '</div>' +

        '<div class="confirmShareView hidden subView"></div>' +
        '<div class="shareListView subView"></div>' +

        '</div>';

        // '<div class="shareSubject"><textarea id="shareAreaAmx">Hola! Te comparto esto :)</textarea></div>';

        var AmxShareDialogView = OC.Backbone.View.extend({

            _templates: {},

            expDate: '',

            configModel: undefined,

            events: {
                'focus .shareWithField': 'onShareWithFieldFocus',
                //'click .shareForm-btn .menuitem label': 'onLabelClick', //FIXME: Why is not triggered automatically
                'click .shareForm-btn .icon-more': 'onToggleMenu',
                'click .shareForm-btn .permissions': 'onPermissionChange',
                'click .shareForm-btn .expireDate' : 'onExpireDateChange',
                //'change .shareForm-btn .datepicker': 'onChangeExpirationDate',
                'click .shareForm-btn .datepicker' : 'showDatePicker'
            },

            initialize: function(options) {

                var view = this;

                // Integrity validations

                this.model.on('fetchError', function() {
                    OC.Notification.showTemporary(t('core', 'Share details could not be loaded for this item.'));
                });

                if(!_.isUndefined(options.configModel)) {
                    this.configModel = options.configModel;
                } else {
                    throw 'missing OC.Share.ShareConfigModel';
                }

                // Events

                this.configModel.on('change:isRemoteShareAllowed', function() {
                    view.render();
                });
                this.model.on('change:permissions', function() {
                    view.render();
                });

                this.model.on('confirmShare', function (model, msg) {

                    view.confirmShare(msg);

                }, this);


                this.model.on('request', this._onRequest, this);
                this.model.on('sync', this._onEndRequest, this);

                // Subviews

                var subViewOptions = {
                    model: this.model,
                    configModel: this.configModel
                };

                var subViews = {
                    shareConfirmView: 'ShareConfirmView',
                    shareListView: 'ShareListView',
                    shareLinkView: 'ShareLinkView'
                };

                for(var name in subViews) {
                    var className = subViews[name];
                    this[name] = _.isUndefined(options[name])
                        ? new OCA.AmxSharing[className](subViewOptions)
                        : options[name];
                }

                // Binding

                _.bindAll(this,
                    'getPermissions',
                    'confirmShare',
                    'setExpirationDate'
                );


            },

            render: function(){

                var $self = this;

                // Create template

                var baseTemplate = this._getTemplate('base', TEMPLATE);

                this.$el.html(baseTemplate({

                    unshareLabel: t('core', 'Unshare'),
                    canShareLabel: t('core', 'Can reshare'),
                    canEditLabel: t('core', 'Can edit'),
                    createPermissionLabel: t('core', 'Can create'),
                    updatePermissionLabel: t('core', 'Can change'),
                    deletePermissionLabel: t('core', 'Can delete'),
                    secureDropLabel: t('core', 'File drop (upload only)'),
                    expireDateLabel: t('core', 'Set expiration date'),
                    passwordLabel: t('core', 'Password protect'),
                    crudsLabel: t('core', 'Access control'),
                    triangleSImage: OC.imagePath('core', 'actions/triangle-s'),
                    isResharingAllowed: this.configModel.get('isResharingAllowed'),
                    isPasswordForMailSharesRequired: this.configModel.get('isPasswordForMailSharesRequired'),
                    sharePermissionPossible: this.model.sharePermissionPossible(),
                    editPermissionPossible: this.model.editPermissionPossible(),
                    createPermissionPossible: this.model.createPermissionPossible(),
                    updatePermissionPossible: this.model.updatePermissionPossible(),
                    deletePermissionPossible: this.model.deletePermissionPossible(),
                    sharePermission: OC.PERMISSION_SHARE,
                    createPermission: OC.PERMISSION_CREATE,
                    updatePermission: OC.PERMISSION_UPDATE,
                    deletePermission: OC.PERMISSION_DELETE,
                    editPermission: OC.PERMISSION_UPDATE,
                    readPermission: OC.PERMISSION_READ,
                    isFolder: this.model.isFolder(),
                    passwordPlaceholder: PASSWORD_PLACEHOLDER_MESSAGE,
                    to: t('amx_filesharing', 'to')

                }));


                // Events

                $("#shareWithAmx").select2_v4({
                    width: '100%',
                    placeholder: t('amx_filesharing', 'writeName'),
                    tags: true,

                    language: {

                        inputTooShort: function () {
                            return t('amx_filesharing', 'writeName');
                        },

                        noResults: function (params) {
                            return t('amx_filesharing', 'noResults');
                        },

                        loadingMore: function () {
                            return t('amx_filesharing', 'loadingMore');
                        },

                        errorLoading: function () {
                            return t('amx_filesharing', 'errorLoading');
                        },

                        searching: function () {
                            return t('amx_filesharing', 'searching');
                        }
                    },

                    tokenSeparators: [',', ' '],

                    dropdownParent: $('div.shareForm-oneline'),
                    ajax: {
                        url: OC.linkToOCS('apps/files_sharing/api/v1') + 'sharees',
                        dataType: 'json',
                        data: function (params) {

                            return {
                                format: 'json',
                                search: params.term.trim(),
                                perPage: 10,
                                itemType: $self.model.get('itemType')
                            };

                        },
                        processResults: function (result) {

                            console.log("Results: ", result);

                            var users   = result.ocs.data.exact.users.concat(result.ocs.data.users);
                            var groups  = result.ocs.data.exact.groups.concat(result.ocs.data.groups);
                            var remotes = result.ocs.data.exact.remotes.concat(result.ocs.data.remotes);
                            var lookup = result.ocs.data.lookup;
                            var emails = [],
                                circles = [];
                            if (typeof(result.ocs.data.emails) !== 'undefined') {
                                emails = result.ocs.data.exact.emails.concat(result.ocs.data.emails);
                            }
                            if (typeof(result.ocs.data.circles) !== 'undefined') {
                                circles = result.ocs.data.exact.circles.concat(result.ocs.data.circles);
                            }

                            var data = $.map(users, function (obj) {
                                return {
                                    id: obj.value.shareWith,
                                    text: obj.label,
                                    type: obj.value.shareType
                                };
                            });

                            // Tranforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data
                            };
                        }

                    },
                    createTag: function (params) {

                        // Don't offset to create a tag if there is no @ symbol
                        if (!OCA.AmxSharing.Util.validateEmail(params.term.trim())) {

                            return null;

                        }

                        return {
                            id: params.term,
                            text: params.term
                        };
                    }
                });


                $("#shareWithAmx").on('change', function (e) {

                    var shareList = $(this).select2_v4('data');

                    if (shareList.length > 0){

                        $self.shareConfirmView.$el.slideDown();
                        $self.shareListView.$el.slideUp();

                    }else{

                        $self.shareConfirmView.$el.slideUp();
                        $self.shareListView.$el.slideDown();

                    }

                    // Do something
                });
                // Extra events

                $('.shareForm-btn .popovermenu').on('beforeHide', function() {

                    var datePickerClass = '.shareForm-btn .expirationDateContainer';
                    var datePickerInput = '.shareForm-btn #expirationDatePicker';
                    var expireDateCheckbox = '.shareForm-btn #expireDate';
                    if ($(expireDateCheckbox).prop('checked')) {
                        $(datePickerInput).removeClass('hidden-visually');
                        $(datePickerClass).removeClass('hasDatepicker');
                        $(datePickerClass + ' .ui-datepicker').hide();
                    }

                });

                this.shareConfirmView.$el = this.$el.find('.confirmShareView');
                this.shareConfirmView.render();

                this.shareListView.$el = this.$el.find('.shareListView');
                this.shareListView.render();

                this.shareLinkView.$el = this.$el.find('.shareLinkView');
                this.shareLinkView.render();


                this.delegateEvents();

            },

            /* ELEMENT EVENTS
            ------------------------------------------------- */

            onLabelClick: function (event) {

                event.preventDefault();
                event.stopPropagation();

                var $element = $(event.target);

                $element.parent().find("input[type='checkbox']").trigger("click");

            },

            onToggleMenu: function(event) {

                event.preventDefault();
                event.stopPropagation();

                var $menu = $('.shareForm-btn .popovermenu');

                OC.showMenu(null, $menu);

            },

            onPermissionChange: function(event){},

            onExpireDateChange: function(event) {

                var element = $(event.target);

                var datePickerClass = '.shareForm-btn .expirationDateContainer';
                var datePicker = $(datePickerClass);
                var state = element.prop('checked');
                datePicker.toggleClass('hidden', !state);

                if (!state) {
                    this.setExpirationDate('');
                } else {
                    this.showDatePicker(event);

                }
            },

            showDatePicker: function(event) {

                var element = $(event.target);

                var expirationDatePicker = '.shareForm-btn #expirationDatePicker';

                var view = this;

                $(expirationDatePicker).closest('div')
                .datepicker($.datepicker.regional[ OC.getLanguageCode(OC.getLocale()) ])
                .datepicker({
                    dateFormat : 'dd-mm-yy',

                    onSelect:
                        function (expireDate) {

                            view.setExpirationDate(expireDate);
                        },

                    onClose:
                        function () {
                            $(expirationDatePicker).removeClass('hidden-visually');
                        }
                });

                $(expirationDatePicker).addClass('hidden-visually');

                // TODO: call setExpirationDate with default datepicker date

            },

            /* VIEW EVENTS
            ------------------------------------------------- */

            getPermissions: function() {

                var permissions = OC.PERMISSION_READ;

                $('.shareForm-btn .permissions').filter(':checked').each(function(index, checkbox) {

                    permissions |=  $(checkbox).data('permissions');

                });

                return permissions;
            },

            setExpirationDate: function(expireDate) {
                this.expDate = expireDate;
            },

            /* VIEW HELPERS
            ------------------------------------------------- */


            confirmShare: function (msg) {

                var $self = this;

                var $shareWithField = $('#shareWithAmx');
                var $shareWithBtn = $('#shareBtnAmx');
                var $shareWithArea = $('#shareAreaAmx');

                var $shareLoading = $('.footer-confirm .icon-loading-small');
                var $shareBar = $('.footer-confirm .progress-bar-back');
                var $shareBarProgress = $('.footer-confirm .progress-bar-front');

                var $shareWithOptions = $('.shareForm-btn input[type=checkbox]');

                // Get share values

                var shareList = $shareWithField.select2_v4('data');

                //var shareList = $shareWithField.val().replace(/,/g, ' ').trim().split(" ");

                if (shareList.length <= 0){ return; }

                // Disable inputs

                $shareWithField.prop("disabled", true);
                $shareWithBtn.prop("disabled", true);
                $shareWithArea.prop("disabled", true);
                $shareWithOptions.prop("disabled", true);

                // Show loading

                $shareLoading.removeClass('hidden').show();
                $shareBar.removeClass('hidden').show();
                $shareBarProgress.css({'width': '0%'});

                var permissions = this.getPermissions();
                var expireDate = this.expDate;

                var shareTotal = shareList.length;

                // Start sharing

                (function doShare(n) {

                    var shareItem = shareList[n];

                    console.log("Trying to share: ", shareItem);

                    if (shareItem === undefined){

                        // Finish sharing state

                        $shareWithField.val('').trigger('change');
                        $shareWithOptions.prop("checked", false);

                        $self.setExpirationDate('');
                        $('.shareForm-btn .expirationDateContainer').toggleClass('hidden', true);
                        $('.shareForm-btn .datepicker').closest('div').datepicker('setDate', null);
                        $('.shareForm-btn .datepicker').val('');

                        $shareWithField.prop("disabled", false);
                        $shareWithBtn.prop("disabled", false);
                        $shareWithArea.prop("disabled", false);
                        $shareWithOptions.prop("disabled", false);

                        $shareLoading.hide();
                        $shareBar.hide();
                        $shareBarProgress.css({'width': '0%'});

                        return;

                    }

                    var item = {
                        shareType: shareItem.type !== undefined ?  shareItem.type : 4,
                        shareWith: shareItem.type !== undefined ? shareItem.id : shareItem.text,
                        permissions: permissions,
                        expireDate: expireDate,
                        subject: msg
                    };

                    console.log("Shared item: ", item);

                    $self.model.addShare(item, {success: function() {

                        $shareBarProgress.css({'width': ( ( (n + 1) / shareTotal) * 100) + '%'});

                        doShare(n + 1);

                    }, error: function(obj, msg) {

                        OC.Notification.showTemporary(msg);

                        $shareBarProgress.css({'width': ( ( (n + 1) / shareTotal) * 100) + '%'});

                        doShare(n + 1);

                    }});

                })(0);


            },

            _getTemplate: function (key, template) {
                if (!this._templates[key]) {
                    this._templates[key] = Handlebars.compile(template);
                }
                return this._templates[key];
            }

        });

    OCA.AmxSharing.ShareDialogView = AmxShareDialogView;

})();


