<?php

$application = new \OCA\AmxFileSharing\AppInfo\Application();

$eventDispatcher = \OC::$server->getEventDispatcher();
$eventDispatcher->addListener(
'OCA\Files::loadAdditionalScripts',
    function() {
        \OCP\Util::addStyle('amx_filesharing', 'mergeAdditionalStyles');
        \OCP\Util::addScript('amx_filesharing', 'additionalScripts');
    }
);


